package vt.cs.project.data;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.bson.Document;

import com.mongodb.client.MongoCursor;

public class FeatureDataFormatter {
	
	public static final String FEATURE_DATASET = "feature_data.csv";
	private static String[] metadataFeatureKeys = new String[] { "_id", "favoriteCount",
		"loveCount", "views", "remixes", "creator_id", "project_order" };

	public static void main(String[] args) {
		DatabaseManager.configure(Constants.HOST_NAME, Constants.DBNAME);
		Map<String, Document> creatorMapByName = new HashMap<>();
		Map<Integer, Document> projectMapByID = new HashMap<>();
		Map<Integer, Document> metadataMapByID = new HashMap<>();

		MongoCursor<Document> creatorDocIter = DatabaseManager.getCreatorIter();

		int currentCreatorID = 1;
		int projectCounter = 1;
		while (creatorDocIter.hasNext()) {
			Document creatorDoc = creatorDocIter.next();
			String creatorName = getCreatorName(creatorDoc);
			List<Document> orderedAnalysisReports = DatabaseManager
					.getOrderedProjectReportsCreatedBy(creatorName);

			//preparing creator data
			creatorMapByName.put(creatorName, creatorDoc);
			
			//preparing project data created by current creator
			for (int reportIdx = 0; reportIdx < orderedAnalysisReports.size(); reportIdx++) {
				Document reportDoc = orderedAnalysisReports.get(reportIdx);
				
				Integer currentProjectID = reportDoc.getInteger("_id");
				projectMapByID.put(currentProjectID, reportDoc);
				
				//prepare metadata
				Document metadataDoc = DatabaseManager
						.findProjectMetadata(currentProjectID);
				//augmented with creator name, and project number
				augmentMetadata(metadataDoc, currentCreatorID, projectCounter);
				metadataMapByID.put(currentProjectID, metadataDoc);
				
				projectCounter++;
			}
			currentCreatorID++;
		}

		// generate output using creator, metadata, smells
		String csvOutput = generateCSVOutput(creatorMapByName,
				projectMapByID, metadataMapByID);
		try {
			FileUtils.writeStringToFile(new File(Constants.OUTPUTDIR, FEATURE_DATASET),
					csvOutput);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void augmentMetadata(Document metadataDoc,
			int currentCreatorID, int projectCounter) {
		metadataDoc.put("creator_id", currentCreatorID);
		metadataDoc.put("project_order", projectCounter);
	}

	public static String getCreatorName(Document creatorDoc) {
		return (String) creatorDoc.get("_id");
	}

	private static String generateCSVOutput(
			Map<String, Document> creatorMapByName,
			Map<Integer, Document> projectMapByID,
			Map<Integer, Document> metadataMapByID) {

		List<Document> docs = extractAndMergeToSingleDoc(
				creatorMapByName, projectMapByID,
				metadataMapByID);

		if (docs.isEmpty()) {
			return "";
		}

		StringBuilder csvOut = new StringBuilder();

		// header
		Set<String> keySetForHeader = docs.get(0).keySet();
		addCSVHeaderUsingKeySet(csvOut, keySetForHeader);

		// body
		for (Document doc : docs) {
			appendCSVEntry(csvOut, keySetForHeader, doc);
		}
		return csvOut.toString();

	}

	public static void appendCSVEntry(StringBuilder csvOut,
			Set<String> keySetForHeader, Document doc) {
		for (String key : keySetForHeader) {
			Object value = doc.get(key);
			if(value==null){
				csvOut.append("");
			}else{
				csvOut.append(value);
			}
			csvOut.append(", ");
		}
		csvOut.append("\n");
	}

	public static void addCSVHeaderUsingKeySet(StringBuilder csvOut,
			Set<String> keySetForHeader) {
		for (String key : keySetForHeader) {
			csvOut.append(key);
			csvOut.append(", ");
		}
		csvOut.append("\n");
	}


	public static List<Document> extractAndMergeToSingleDoc(
			Map<String, Document> creatorMapByName,
			Map<Integer, Document> projectMapByID,
			Map<Integer, Document> metadataMapByID) {
		
		List<Document> outputDocs = new ArrayList<>();
		Document mergedKeyValueDoc = new Document();
		for (Integer projectID : projectMapByID.keySet()) {
			Document analysisReportDoc = projectMapByID.get(projectID);
			Document analysisSubEntry = extractAnalysisValues(analysisReportDoc);
			
			Document metadataDoc = metadataMapByID.get(projectID);
			Document metadataSubEntry = extractMetadataValues(metadataDoc);
			
			Document additionalInfoSubEntry = new Document();
			String creatorName = metadataDoc.getString("creator");
			Document creator = creatorMapByName.get(creatorName);
			additionalInfoSubEntry.append("joined", (Number) creator.get("joined"));
			
			mergedKeyValueDoc = combineDoc(
					combineDoc(analysisSubEntry, metadataSubEntry),
					additionalInfoSubEntry);

			outputDocs.add(mergedKeyValueDoc);
		}
		return outputDocs;
	}

	static Document combineDoc(Document doc1, Document doc2) {
		Document result = new Document();
		for (String key : doc1.keySet()) {
			result.append(key, doc1.get(key));
		}
		for (String key : doc2.keySet()) {
			result.append(key, doc2.get(key));
		}
		return result;
	}

	static Document extractMetadataValues(Document metadataDoc) {
		Document outputDoc = new Document();
		for (String key : metadataFeatureKeys) {
			outputDoc.append(key, (Number) metadataDoc.get(key));
		}
		return outputDoc;
	}

	static Document extractAnalysisValues(Document report) {
		Document outputDoc = new Document();

		extractSmells(report, outputDoc);
		extractCT(report, outputDoc);
		extractMetrics(report, outputDoc);

		return outputDoc;
	}

	public static void extractMetrics(Document report, Document outputDoc) {
		Document metrics = (Document) report.get("metrics");
		Document PS = (Document) metrics.get("PS");
		String[] keyPS = new String[] { "bloc", "sprite", "script" };
		for (String key : keyPS) {
			Number value = (Number) PS.get(key);
			outputDoc.put(key, value);
		}
		Document PE = (Document) metrics.get("PE");
		
		Document CC = (Document) metrics.get("CC");
		outputDoc.put("CC", CC.getDouble("mean"));
		
		String[] keyPE = new String[] { "customBlockCount", "varCount",
				"commentCount", "cloneCount"};
		for (String key : keyPE) {
			Number value = (Number) PE.get(key);
			outputDoc.put(key, value);
		}
		
		
	}

	public static void extractCT(Document report, Document outputDoc) {
		Document ctConcepts = (Document) report.get("cumulativeCT");
		for (String ct : ctConcepts.keySet()) {
			outputDoc.put(ct, ctConcepts.get(ct));
		}
	}

	public static void extractSmells(Document report, Document outputDoc) {
		for (String smell : Constants.SMELLS) {
			Double smellDensity = getSmellDensity(smell, report);
			outputDoc.put(smell, smellDensity);
		}
	}

	private static double[] convertToDoubleArr(List<Double> doubleList) {
		double[] target = new double[doubleList.size()];
		for (int i = 0; i < target.length; i++) {
			target[i] = doubleList.get(i);
		}
		return target;
	}

	public static Double getSmellDensity(String attributeName, Document d) {
		Document smellReport = (Document) d.get("smells");
		Document smell = (Document) smellReport.get(attributeName);
		Number smellCount = (Number) smell.get("count");
		if(smellCount == null){
			return null;
		}
		
		//project size
		Document metricReport = (Document) d.get("metrics");
		Document PS = (Document) metricReport.get("PS");
		Number projectSize = (Number) PS.get("bloc");
		double density = smellCount.doubleValue()/(projectSize.doubleValue()/100.0);
		return density;
	}

}
