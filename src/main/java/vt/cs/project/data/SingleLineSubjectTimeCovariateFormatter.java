package vt.cs.project.data;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ArrayUtils;
import org.bson.Document;

import vt.cs.smells.datamanager.crawler.Creator;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;

public class SingleLineSubjectTimeCovariateFormatter {
	private static final int maximumEventConsidered = 4;
	private static final String targetSmell = "BVS";
	private static final double percentileForHighRisk = 70.0;
	public static FlatDocTransformer transformer;
	private static ComputeStatInfo compute;
	static TemporalDataExtractor extractor;
	private static String[] attributesForMedians = new String[]{"sprite","script","varCount","CC"};
	private static String[] attributesForSum = new String[]{"favoriteCount", "loveCount",
			"views","remixes", "bloc", "customBlockCount","commentCount", "cloneCount"};
	
	static int maxProject = 10;
	
	private static String[] analysisAttributes = new String[]{"stime","futime"};
	public static void main(String[] args){
		initialization();
		
		String[] attributesConsidered = (String[])ArrayUtils.addAll(attributesForMedians, attributesForSum);
		Map<String, Double> thresholdMap = extractSmellDensityThreshold();
		System.out.println(thresholdMap);
		Document structure = DatabaseManager.getDocumentStructure();
		Document header = buildHeader(attributesConsidered, structure);
		List<Document> entries = new ArrayList<>();
		
		MongoCursor<Document> creatorDocIter = DatabaseManager.getCreatorIter();
		int projCounter = 0;
		int maxEvent = 0;
		while (creatorDocIter.hasNext()) {
			Document creatorDoc = creatorDocIter.next();
			int NUM_BASELINE_PROJECTS = 15;//randomWithRange(3,10); //create variability of programmer backgrounds
			String creatorName = (String) creatorDoc.get("_id");
			System.out.println(creatorName);
			System.out.println(projCounter);
			
			List<Document> projectReports = DatabaseManager
					.getOrderedProjectReportsCreatedBy(creatorName);
			
			if(projectReports.size()<NUM_BASELINE_PROJECTS){
				continue;
			}
			
			
			Document smells = (Document) structure.get("smells");
			String[] smellKeys = (String[]) smells.keySet().toArray(new String[smells.size()]);
			Map<String, Double> baselineMedianSmell = compute.calculateMedian(creatorName, NUM_BASELINE_PROJECTS, smellKeys, projectReports);
			
			Map<String, Double> baselineByMedian = compute.calculateMedian(creatorName, NUM_BASELINE_PROJECTS, attributesForMedians,projectReports);
			Map<String, Double> baselineBySum= compute.calculateSum(creatorName, NUM_BASELINE_PROJECTS, attributesForSum, projectReports);
		
			Map<String, Double> baselineValues = new HashMap<>();
			baselineValues.putAll(baselineByMedian);
			baselineValues.putAll(baselineBySum);
			
			
			Document entry = new Document();
			projCounter =  projCounter+1;
			
			try{
				//construct entry
				entry.put("id", projCounter);
				for(String key: attributesConsidered){
					entry.put(key, baselineValues.get(key));
				}
				
				//put baseline smells (median)
				for(String key: baselineMedianSmell.keySet()){
					entry.put(key, baselineMedianSmell.get(key));
				}
				
				Document lastEntryBeforeStart = getLastEntryBeforeStart(projectReports, NUM_BASELINE_PROJECTS);
				//cumulative CT
				Document lastCumulativeCT = (Document) lastEntryBeforeStart.get("cumulativeCT");
				for(String key: lastCumulativeCT.keySet()){
					entry.put(key,lastCumulativeCT.get(key));
				}
				
				//get project id
				int projectID = lastEntryBeforeStart.getInteger("_id");
				
				//time since joined
				Document metadata = DatabaseManager.findProjectMetadata(projectID);
				Date dateShared = metadata.getDate("dateShared");
				LocalDate dateSharedLocal = new java.sql.Date(dateShared.getTime()).toLocalDate();
				Date joinedDate = creatorDoc.getDate("joinedDate");
				LocalDate joinedDateLocal = new java.sql.Date(joinedDate.getTime()).toLocalDate();
				double yearExperience = Creator.yearElapsed(joinedDateLocal, dateSharedLocal);
				entry.put("exp", yearExperience);
				
				
				//cumulative BR
				Document lastCumulativeBR = (Document) lastEntryBeforeStart.get("cumulativeBR");
				for(String key: lastCumulativeBR.keySet()){
					entry.put(key,lastCumulativeBR.get(key));
				}
				
				//stime
				entry.put("stime", NUM_BASELINE_PROJECTS);
				
				//futime
//				entry.put("futime", projectReports.size()-NUM_BASELINE_PROJECTS+1);

				//events
				Map<String, List<Integer>> smellEventMap = extractHightRiskSmellEventData(creatorName,thresholdMap, NUM_BASELINE_PROJECTS);
				List<Integer> events = smellEventMap.get(targetSmell);
//				maxEvent = Math.max(maxEvent, events.size());
				maxEvent = Math.min(maximumEventConsidered, events.size());
				
				if(maxEvent>0){
					if(events.get(maxEvent-1) < maxProject){
						entry.put("futime", events.get(maxEvent-1)+1);
//						entry.put("futime", maxProject);
					}else{
						entry.put("futime", maxProject);
					}
				}else{
//					check how many projects created pass 15
					int numProjectCreatedAfterBaseLine = projectReports.size()-NUM_BASELINE_PROJECTS+1;
					int futime = Math.min(maxProject, numProjectCreatedAfterBaseLine);
					entry.put("futime", futime);
				}
				
				for(int ei = 0; ei < maxEvent; ei++){
//				for(int ei = 0; ei < events.size(); ei++){
					if(events.get(ei) < maxProject){
						entry.put("etime"+(ei+1), events.get(ei));
					}
					
				}
				
				entries.add(entry);
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}
		
		//add etime to header
		for(int i = 0; i < maximumEventConsidered; i++){
			header.put("etime"+(i+1), null);
		}
		
		//generate CSV
		StringBuilder csvOutput = new StringBuilder();
		String prefix = "";
		//header first
		for(String key: header.keySet()){
			csvOutput.append(prefix);
			csvOutput.append(key);
			prefix = ",";
		}
		csvOutput.append("\n");
		
		//entries
		
		for(Document entry: entries){
			prefix = "";
			for(String key: header.keySet()){
				csvOutput.append(prefix);
				Object val = entry.get(key);
				if(val==null){
					csvOutput.append("");
				}else{
					csvOutput.append(val);
				}
			
				prefix = ",";
				
			}
			csvOutput.append("\n");
		}
		
		
		//writing to a file
		try {
			FileUtils.writeStringToFile(new File(Constants.OUTPUTDIR, targetSmell+"_cox_data.csv"), csvOutput.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public static Document buildHeader(String[] attributesConsidered, Document structure) {
		Document header = new Document();
		//build header structure
		header.put("id", null);
		for(String key: attributesConsidered){
			header.put(key, null);
		}
		
		//smells
		Document smells = (Document) structure.get("smells");
		for(String key: smells.keySet()){
			header.put(key, null);
		}
		
		//experience
		header.put("exp", null);
		
		//BR
		Document BR = (Document) structure.get("cumulativeBR");
		for(String key: BR.keySet()){
			header.put(key, null);
		}
		
		//CT
		Document CT = (Document) structure.get("cumulativeCT");
		for(String key: CT.keySet()){
			header.put(key, null);
		}
		
		//Analysis
		for(String key: analysisAttributes){
			header.put(key, null);
		}
		
		return header;
	}


	public static Map<String, Double> extractSmellDensityThreshold() {
		Map<String, Double> thresholdMap = new HashMap<>();
		//compute percentiles of smell density
		for(String smell : Constants.SMELLS){
			Double value = compute.findPercentile(percentileForHighRisk,smell);
			thresholdMap.put(smell, value);
		}
		return thresholdMap;
	}


	public static void initialization() {
		DatabaseManager.configure(Constants.HOST_NAME, Constants.DBNAME);
		transformer = new FlatDocTransformer();
		compute = new ComputeStatInfo();
		extractor = new TemporalDataExtractor();
	}


	private static Document getLastEntryBeforeStart(
			List<Document> projectReports, int num_baseline_project) {
		return projectReports.get(num_baseline_project-1);
		
	}


	private static Map<String, List<Integer>> extractHightRiskSmellEventData(String creatorName, Map<String, Double> thresholdMap, int beginObserving) {
		Map<String,List<Integer>> highRiskProjects = extractor.getProjectNumberHigherThanThreshold(creatorName, Constants.SMELLS,thresholdMap, beginObserving);
		return highRiskProjects;
	}
	
	static int randomWithRange(int min, int max)
	{
	   int range = (max - min) + 1;     
	   return (int)(Math.random() * range) + min;
	}
	
}
