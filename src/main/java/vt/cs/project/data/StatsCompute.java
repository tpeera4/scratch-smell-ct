package vt.cs.project.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.descriptive.rank.Median;
import org.bson.Document;

import cern.colt.Arrays;

public class StatsCompute {

	private List<Document> inputReport;

	public void setInputProjects(List<Document> reports) {
		this.inputReport = reports;

	}

//	public Document getMaxForCT() {
//		Document result = new Document();
//		Map<String, ArrayList<Number>> statsDict = new HashMap<String, ArrayList<Number>>();
//		String[] consideredCT = new String[]{"FlowControl","abstraction", "DataRepresentation", "Logic", "Synchronization", "Parallelization"};
//		for (Document report : inputReport) {
//			Document metrics = (Document) report.get("metrics");
//			Document MS = (Document) metrics.get("MS");
//			
//			for (String key : consideredCT) {
//				statsDict.putIfAbsent(key, new ArrayList<Number>());
//				statsDict.get(key).add(MS.getInteger(key));
//			}
//		}
//
//		double totalConsidered = 0;
//		for (String key : statsDict.keySet()) {
//			DescriptiveStatistics stats  = new DescriptiveStatistics();
//			double[] values = new double[statsDict.get(key).size()];
//			for (int i = 0; i < values.length; i++) {
//				stats.addValue(statsDict.get(key).get(i).doubleValue());
//			}
//			result.append(key, stats.getMax());
//			totalConsidered += stats.getMax();
//		}
//
//		result.append("total", totalConsidered);
//		
//		return result;
//	}

	public Document getMedianForSmells() {
		Document result = new Document();
		Map<String, ArrayList<Number>> statsDict = new HashMap<String, ArrayList<Number>>();
		for (Document report : inputReport) {
			Document smells = (Document) report.get("smells");

			for (String key : smells.keySet()) {
				statsDict.putIfAbsent(key, new ArrayList<Number>());
				Document smellReport = (Document) smells.get(key);
				Integer count = smellReport.getInteger("count");
				if(count!=null){
					statsDict.get(key).add(count);
				}
				
			}
		}
		
		for (String key : statsDict.keySet()) {
			Median median = new Median();
			double[] values = new double[statsDict.get(key).size()];
			for (int i = 0; i < values.length; i++) {
				values[i] = statsDict.get(key).get(i).doubleValue();
			}
			result.append(key, median.evaluate(values));
			System.out.println("Smell "+key+ ":"+Arrays.toString(values));
		}
		
		return result;
	}

}
