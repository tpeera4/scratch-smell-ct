package vt.cs.project.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.Document;


public final class Constants {

	static final String DBNAME = "scratch-analytics2";

	static final String HOST_NAME = "localhost";
	public static String OUTPUTDIR = "../StatisticalAnalysis/";
	static String[] SMELLS = new String[] { "HCMS", "UV", "UW", "LS",
	"EFGS", "UC", "DS", "UCB", "UBC", "UN", "BVS", "US", "DC" };

	public static final int START_PROJECT_ID = 41401820;
	public static final int END_PROJECT_ID = 96131820;

	static String QUALIFIED_CREATORS = "beanthecat, Atari764, SpiritTracks123, urboss, urboss, Wildflight, _youtubeN1, FleeingPrism31, 2088, Careq2005, minicatty, is77, chavis97, ko2222, DulzKansekarin, TailsShadowAshby, Careq2005, _paperN1, Jasmine76, Mewkitten33, PerfectKitty, jon606, digthebone, gagdety, BillyBobson, piggy2272, jon606, jL3000, Scratchycat4000, Mr_pigy123, BIGsmoog98, JoshyRobot, forwhiledo, ScritchyGScratch, marek12306, Enderking1230, yoshi1229, dudec2004, dudec2004, pig38, Kidplayer111, harrymcneill, NuclearBlaster, shunart, marek12306, TheJellyConference, ko2222, ysk1108, ionvop, IAmAFunnyMonkey, 456as123, challotepr, Zetko, 456as123, 456as123, No3DSXL, 456as123, greenblue1223, EpicDudeOfEpicness, Gobo18, marek12306, 456as123, 456as123, chavis97, zinfindel, hauntedmonsteroonie, 50zeroWeirdo, iceninja1234567, jdfr03, The_Grits, jexpress, Isadovenko, whitefox77, chavis97, PigwaiilTunes, JK14, ViniTheGamer, mineskaper, burnwolf1650, UltimatetheHedgie, The_Grits, challotepr, Pipko411, 123hasan, nicoclack, _youtubeN1, JK14, bombkirby13, JK14, Mr_pigy123, brandomn8999, beanthecat, MegaApuTurkUltra, MTCGreen";

	public static final double percentileForHighRisk = 70.0;

	static final int MINIMUM_BLOC = 10;

	static String CREATOR_OUT_DIR = OUTPUTDIR;

	static Set<String> CREATOR_SET;

	static String CREATOR_LIST_FILENAME = "creator_list";


}
