package vt.cs.project.data;

import org.bson.Document;

public class FlatDocTransformer {

	public Document flattenAnalysis(Document d) {
		return FeatureDataFormatter.extractAnalysisValues(d);
	}

	public Document flattenMeta(Document metaReport) {
		return FeatureDataFormatter.extractMetadataValues(metaReport);
	}

	public Document merge(Document flatAnalysisReport, Document flatMetaReport) {
		return FeatureDataFormatter.combineDoc(flatAnalysisReport,flatMetaReport);
	}

}
