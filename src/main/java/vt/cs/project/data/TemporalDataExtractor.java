package vt.cs.project.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;

import vt.cs.smells.datamanager.crawler.Creator;

public class TemporalDataExtractor {
	
	
	public List<Double> extractSmellDensity(String creatorName, String attributeName) {
		List<Document> projectReports = DatabaseManager
				.getOrderedProjectReportsCreatedBy(creatorName);
		
		List<Double> result = new ArrayList<>();
		for(Document d : projectReports){
			Double density = FeatureDataFormatter.getSmellDensity(attributeName, d);
			result.add(density);
		}
		return result;

	}
	
	private List<Integer> getProjectNumHigherThanThreshold(String creatorName,
			double threshold, String attribute, int beginObserving) {
		List<Double> smellDensityValues = extractSmellDensity(creatorName, attribute);

		List<Integer> projectNumbers = new ArrayList<>();
		for(int i = beginObserving ; i < smellDensityValues.size(); i++){
			Double smellDensityVal = smellDensityValues.get(i);
			if(smellDensityVal!=null && smellDensityVal > threshold){
				projectNumbers.add(i-beginObserving+1);
			}
		}
		
		return projectNumbers;
		
	}
	
	public Map<String, List<Integer>> getProjectNumberHigherThanThreshold(String creatorName, String[] attributes, Map<String,Double> thresholdMap, int beginObserving){
		Map<String, List<Integer>> results = new HashMap<>();
		for(String attribute: attributes){
			Double threshold = thresholdMap.get(attribute);
			List<Integer> projectNumHighRisk = getProjectNumHigherThanThreshold(creatorName, threshold, attribute, beginObserving);
			results.put(attribute, projectNumHighRisk);
		}
		
		return results;
	}
	


}
