package vt.cs.project.data;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.bson.Document;

import vt.cs.smells.analyzer.AnalysisManager;
import vt.cs.smells.datamanager.crawler.Crawler;
import vt.cs.smells.datamanager.crawler.Creator;
import vt.cs.smells.datamanager.crawler.ProjectMetadata;

public class DataCollector {
	static Logger logger = Logger.getLogger(DataCollector.class);
	public static void main(String[] args){
		
		DatabaseManager.configure(Constants.HOST_NAME, Constants.DBNAME);
		Set<String> creators = retrieveCreatorFromFile();
		logger.info("Total creators to be updated: "+ creators.size());
		
		int counter = 0;
		for(String creatorName: creators){
			//check if creator name already exists 
			if(DatabaseManager.findCreatorRecord(creatorName)!=null){
				continue;
			}
			//retrieve creator metadata
			Creator creator = CreatorQuery.retrieveCreatorMetadata(creatorName);
		
			//retrieve list of original projects
			List<Integer> allProjectsCreated = CreatorQuery.extractProjectList(creator);
			HashMap<Integer, ProjectMetadata> originalProjects = CreatorQuery.extractOriginalProjectList(allProjectsCreated);
			logger.info("projects created:"+originalProjects.size());
			collectProjectsByCreator(creatorName, originalProjects);
			
			//save creator after retrieve project list
			DatabaseManager.putCreatorRecord(creator);
			counter++;
			
			logger.info(creatorName);
			logger.info(counter + "/" + creators.size());
		}
	}

	static void collectProjectsByCreator(String creatorName, HashMap<Integer, ProjectMetadata> originalProjects) {
		Map<Integer, Document> projectIDToReport = new HashMap<>();
		//process each project
		Map<Integer, String> projectIDToSrc = new HashMap<>();
		List<Integer> nonTrivialProjects;
		for(Integer id: originalProjects.keySet()){
			ProjectMetadata metadata = originalProjects.get(id);
			if(DatabaseManager.findSource(id) == null){
				//retrieve source 
				try {
					String src = Crawler.retrieveProjectSourceFromProjectID(id);
					projectIDToSrc.put(id, src);
				} catch (Exception e) {}
				//retrieve report
				try{
					AnalysisManager analyzer = new AnalysisManager();
					analyzer.analyze(projectIDToSrc.get(id));
					Document report = Document.parse(analyzer.getConciseJSONReports().toJSONString());
					projectIDToReport.put(id, report);
				} catch (Exception e) {}
			}
		}
		
		//filter trivial projects
		nonTrivialProjects = CreatorQuery.extractNonTrivialProjectList(projectIDToSrc, projectIDToReport);
		
		//save metadata, src and report to database
		logger.info("post-filtered project collected:" + nonTrivialProjects.size());
		for (Integer projectID : nonTrivialProjects){
			DatabaseManager.putProjectMetadataRecord(originalProjects.get(projectID));
			DatabaseManager.putSource(projectID, projectIDToSrc.get(projectID));
			DatabaseManager.putAnalysisReport(projectID, projectIDToReport.get(projectID));	
		}
	}
	
	public static List<String> getCreatorNameList() {
		List<String> splitted = Arrays.asList(Constants.QUALIFIED_CREATORS.split(","));
		List<String> creators = new ArrayList<>();
		splitted.forEach(name -> creators.add(name.trim()));
		return creators;
	}
	

	static Set<String> retrieveCreatorFromFile() {
		Set<String> result = null;
		try {
			List<String> creators = FileUtils.readLines(new File(Constants.CREATOR_OUT_DIR, Constants.CREATOR_LIST_FILENAME));
			result = new HashSet<String>(creators);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	static void appendCreatorIfNotExist(String creatorName){
		try {
			Constants.CREATOR_SET = retrieveCreatorFromFile();
			if(!Constants.CREATOR_SET.contains(creatorName)){
				FileUtils.writeStringToFile(new File(Constants.CREATOR_OUT_DIR, Constants.CREATOR_LIST_FILENAME), creatorName+"\n", true);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static void setOuputConfig(String outputDir, String filename) {
		Constants.CREATOR_OUT_DIR =  outputDir;
		Constants.CREATOR_LIST_FILENAME = filename;
	}
}
