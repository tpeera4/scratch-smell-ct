package vt.cs.project.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import vt.cs.smells.analyzer.AnalysisManager;
import vt.cs.smells.datamanager.crawler.Crawler;
import vt.cs.smells.datamanager.crawler.Creator;
import vt.cs.smells.datamanager.crawler.ProjectMetadata;
import vt.cs.smells.datamanager.crawler.RetryOnException;

public class CreatorQuery {
	private static final String BASE_SCRATCH_CREATOR_PROJECT_URL = "https://scratch.mit.edu/users/%s/projects/";
	private static final String profileBaseURL = "https://scratch.mit.edu/users/%s";
	static SimpleDateFormat parser =new SimpleDateFormat("yyyy-MM-dd");
	
	public static Creator retrieveCreatorMetadata(String creatorName){

		Creator creator = new Creator(creatorName);
		RetryOnException retry = new RetryOnException(3, 2000);
		org.jsoup.nodes.Document doc = null;
		String profilePageURL = String.format(profileBaseURL, creatorName);
		while (retry.shouldRetry()) {
			try {
				doc = Jsoup.connect(profilePageURL).get();
				break;
			} catch (Exception e) {
				try {
					retry.errorOccured();
				} catch (Exception failAttemptException) {
					// throw new RuntimeException("Exception while calling URL:"
					// + projectPageURL, failAttemptException);
					// logger.error("Error retrieving metadata for project: "+
					// metadata.getProjectID() + "...skipping..." );
					return null;
				}
			}
		}

		extractJoinedDate(creator, doc);
		
		
		
		return creator;
		

	}
	
	
	

	static List<Integer> extractNonTrivialProjectList(
			Map<Integer, String> projectIDToSrc, Map<Integer, Document> projectIDToReport) {
		
		List<Integer> results = new ArrayList<>();
		
		for(Integer ID: projectIDToSrc.keySet()){
			try {
				Document report = projectIDToReport.get(ID);
				Document metrics = (Document) report.get("metrics");
				Document PS = (Document) metrics.get("PS");
				double bloc = PS.getDouble("bloc");
				if(bloc > Constants.MINIMUM_BLOC){
					results.add(ID);
				}
				
				

			} catch (Exception e) {}
		}
		
		return results;
	}

	public static List<Integer> extractProjectList(Creator creator) {
		List<Integer> results = new ArrayList<Integer>();
		RetryOnException retry = new RetryOnException(3, 2000);
		org.jsoup.nodes.Document doc = null;
		String projectPageURL = String.format(BASE_SCRATCH_CREATOR_PROJECT_URL, creator.getName());
		while (retry.shouldRetry()) {
			try {
				doc = Jsoup.connect(projectPageURL).get();
				break;
			} catch (Exception e) {
				try {
					retry.errorOccured();
				} catch (Exception failAttemptException) {
					// throw new RuntimeException("Exception while calling URL:"
					// + projectPageURL, failAttemptException);
					// logger.error("Error retrieving metadata for project: "+
					// metadata.getProjectID() + "...skipping..." );
					return results;
				}
			}
		}

		Elements projectElements = doc.select("span.title");
		for(Element el: projectElements){
			String linkStr = el.getElementsByTag("a").attr("href");
			String projectIDStr = linkStr.substring(10,linkStr.length()-1).trim();
			Integer projectID = Integer.parseInt(projectIDStr);
			results.add(projectID);
		}
		
		return results;
	}

	public static void extractJoinedDate(Creator creator,
			org.jsoup.nodes.Document doc) {
		String joinDateStr = doc.select("p.profile-details").get(0).getElementsByTag("span").get(1).attr("title");
		
		Date joinDate = null;
		try {
			joinDate = parser.parse(joinDateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		creator.setJoinDate(joinDate);
		
	}
	
	public static HashMap<Integer, ProjectMetadata> extractOriginalProjectList(List<Integer> projects) {
		HashMap<Integer, ProjectMetadata> originalProjects = new HashMap<Integer,ProjectMetadata>();
		for(Integer projectID : projects){
			ProjectMetadata metadata = new ProjectMetadata(projectID);
			try {
				Crawler.retrieveProjectMetadata(metadata);
				if(metadata.isOriginal()){
					originalProjects.put(projectID, metadata);
				}
			} catch (Exception e) {	}
			
		}
		return originalProjects;
	}


}
