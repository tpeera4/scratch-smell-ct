package vt.cs.project.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.stat.descriptive.rank.Median;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;
import org.bson.Document;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;

public class ComputeStatInfo {
	
	FindIterable<Document> retrievedListOfCreators;
	public ComputeStatInfo() {
		retrievedListOfCreators = DatabaseManager.getAllCreators();
	}

	public Double findPercentile(double percentile, String smellName) {
		
		MongoCursor<Document> creatorDocIter = retrievedListOfCreators
				.iterator();

		// gathering all projects
		List<Document> allProjects = new ArrayList<Document>();
		while (creatorDocIter.hasNext()) {
			Document creatorDoc = creatorDocIter.next();
			String creatorName = (String) creatorDoc.get("_id");
			List<Document> projectReports = DatabaseManager
					.getOrderedProjectReportsCreatedBy(creatorName);
			allProjects.addAll(projectReports);

		}
		// now analyze percentile
		List<Double>validDensityVals = new ArrayList<>();
		for (int i = 0; i < allProjects.size(); i++) {
			Double smellDensity = FeatureDataFormatter.getSmellDensity(smellName,
					allProjects.get(i));
			if(smellDensity!=null){
				validDensityVals.add(smellDensity);
			}
			
		}
		
		double[] validDensityValsArray = new double[validDensityVals.size()];
		for(int i =0; i < validDensityVals.size();i++){
			validDensityValsArray[i] = validDensityVals.get(i);
		}

		Percentile percentileObj = new Percentile();
		double percentileValue = percentileObj.evaluate(validDensityValsArray, percentile);

		return percentileValue;
	}

	public Map<String, Double> calculateMean(String creatorName, int windowSize,
			String[] attributes, List<Document> projectReports) {
		
		int projectCount = 0;
		
		Map<String, Double> attributeSum = new HashMap<>();
		
		
		for (int reportIdx = 0; reportIdx < projectReports.size(); reportIdx++) {
			Document reportDoc = projectReports.get(reportIdx);
			Integer currentProjectID = reportDoc.getInteger("_id");
			Document metadataDoc = DatabaseManager
					.findProjectMetadata(currentProjectID);
			
			Document doc = mergeAnalysisReportAndMetadata(reportDoc,
					metadataDoc);
			
			for(String attrKey : attributes){
				attributeSum.putIfAbsent(attrKey, 0.0);
				Number current = attributeSum.get(attrKey);
				Number double1 = (Number) doc.get(attrKey);
				attributeSum.put(attrKey, current.doubleValue()+double1.doubleValue());
				
			}
			
			projectCount++;
			if(projectCount >= windowSize){
				break;
			}
		}
		
		for(String attrKey : attributes){
			Double current = attributeSum.putIfAbsent(attrKey, 0.0);
			if(current!=null){
				attributeSum.put(attrKey, current/windowSize);
			}
		}
		
		return attributeSum;

	}

	public Map<String, Double> calculateSum(String creatorName,
			int windowSize, String[] attributes,List<Document> projectReports) {
		
		int projectCount = 0;
		Map<String, Double> runningAttributeSum = new HashMap<>();
		
		for (int reportIdx = 0; reportIdx < projectReports.size(); reportIdx++) {
			Document reportDoc = projectReports.get(reportIdx);
			Document metadataDoc = DatabaseManager
					.findProjectMetadata(reportDoc.getInteger("_id"));
			Document combinedDoc = mergeAnalysisReportAndMetadata(reportDoc,
					metadataDoc);
			
			for(String attrKey : attributes){
				runningAttributeSum.putIfAbsent(attrKey, 0.0);
				Number current = runningAttributeSum.get(attrKey);
				Number newVal = (Number) combinedDoc.get(attrKey);
				runningAttributeSum.put(attrKey, current.doubleValue()+newVal.doubleValue());
			}
			
			projectCount++;
			if(projectCount >= windowSize){
				break;
			}
		}
		
		//filled with 0.0 if no attribute found
		for(String attrKey : attributes){
			if(!runningAttributeSum.containsKey(attrKey)){
				runningAttributeSum.put(attrKey, 0.0);
			}
		}
		
		return runningAttributeSum;

	}

	public Map<String, Double> calculateMedian(String creatorName,
			int windowSize, String[] attributes, List<Document> projectReports) {

		int projectCount = 0;
		Map<String, Double> attributeMedian = new HashMap<>();
		Map<String, Number[]> attributesData = new HashMap<>();
		
		for (int reportIdx = 0; reportIdx < projectReports.size(); reportIdx++) {
			Document reportDoc = projectReports.get(reportIdx);
			Document metadataDoc = DatabaseManager.findProjectMetadata(reportDoc.getInteger("_id"));
			Document combinedDoc = mergeAnalysisReportAndMetadata(reportDoc,metadataDoc);
			
			for(String attrKey : attributes){
				attributesData.putIfAbsent(attrKey, new Number[windowSize]);
				Number[] current = attributesData.get(attrKey);
				current[reportIdx] = (Number) combinedDoc.get(attrKey);
				attributesData.put(attrKey, current);
			}
			
			projectCount++;
			
			if(projectCount >= windowSize){
				break;
			}
		}
		
		for(String attrKey : attributes){
			Median median = new Median();
			List<Double> valuesList = new ArrayList<>();
			for(int i = 0; i< windowSize; i++){
				Number value = attributesData.get(attrKey)[i];
				if(value!=null){
					valuesList.add(value.doubleValue());
				}
				
			}
			
			double[] valuesArray = new double[valuesList.size()];
			for(int i = 0; i < valuesList.size(); i++){
				valuesArray[i] = valuesList.get(i);
			}
			
			Double medianVal = median.evaluate(valuesArray,Constants.percentileForHighRisk);
			Double current = attributeMedian.putIfAbsent(attrKey,medianVal);
			if(current!=null){
				attributeMedian.put(attrKey, current);
			}
		}
		
		return attributeMedian;
	}

	public Document mergeAnalysisReportAndMetadata(Document reportDoc,
			Document metadataDoc) {
		FlatDocTransformer transformer = new FlatDocTransformer();
		Document doc = transformer.merge(transformer.flattenAnalysis(reportDoc), 
				transformer.flattenMeta(metadataDoc));
		return doc;
	}

}
