package vt.cs.project.data;
import static com.mongodb.client.model.Filters.eq;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.bson.Document;
import org.json.simple.JSONObject;

import vt.cs.smells.datamanager.crawler.Creator;
import vt.cs.smells.datamanager.crawler.ProjectMetadata;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

public class DatabaseManager {
	private static final String TEST_SCRATCH_ANALYTICS = "test-scratch-analytics";
	private static final String METADATA_COLLECTION_NAME = "metadata";
	private static final String REPORT_COLLECTION_NAME = "smells";
	private static final String METRICS_COLLECTION_NAME = "metrics";
	private static final String CREATOR_COLLECTION_NAME = "creators";
	private static final String SOURCE_COLLECTION_NAME = "sources";
	private static MongoDatabase db = null;
	private static MongoClient mongoClient;
	private static String MONGOEXPORT_BIN ="";
	private static boolean TEST = false;
	

	public static void configure(String host, String dbName) {
		mongoClient = new MongoClient(host);
		db = mongoClient.getDatabase(dbName);
		db.getCollection("test").createIndex(new Document("testdata", "text"));
	}

	//need recent replication of database by 
	//use test-scratch-analytics
	//db.runCommand( { dropDatabase: 1 } )
	//executing on console db.copyDatabase("scratch-analytics", "test-scratch-analytics") 
	public static void configureToTest() {
		configure("localhost","test-scratch-analytics");
	}


	public void dropDatabase() {
		db.drop();
		
	}
	
	// Creator
	public static void putCreatorRecord(Creator creator) {
		Document matchedRecord = findCreatorRecord(creator.getName());
		if(matchedRecord == null){
			db.getCollection(CREATOR_COLLECTION_NAME).insertOne(creator.toDocument());
		}else{
			db.getCollection(CREATOR_COLLECTION_NAME).findOneAndReplace(eq("_id", creator.getName()), creator.toDocument());
		}
	}

	static Document findCreatorRecord(String name) {
		FindIterable<Document> iterable = db.getCollection(CREATOR_COLLECTION_NAME).find(eq("_id", name));
		return iterable.first();	
	}

	// Project Metadata

	public static void putProjectMetadataRecord(ProjectMetadata metadata) {
		int projectID = metadata.getProjectID();
		if(findProjectMetadata(projectID) == null){
			db.getCollection(METADATA_COLLECTION_NAME).insertOne(metadata.toDocument());
		}else{
			db.getCollection(METADATA_COLLECTION_NAME).findOneAndReplace(eq("_id", projectID), metadata.toDocument());
		}
	}
	
	
	public static Document findProjectMetadata(int projectID) {
		FindIterable<Document> iterable = db.getCollection(METADATA_COLLECTION_NAME).find(eq("_id", projectID));
		if(iterable==null){
			return null;
		}else{
			return iterable.first();
		}	
	}
	
	//SOURCE
	
	public static void putSource(int projectID, String src) {
		Document source = new Document() ;
		source.append("_id", projectID);
		source.append("src", src);
		
		if(findSource(projectID) == null){
			db.getCollection(SOURCE_COLLECTION_NAME).insertOne(source);
		}else{
			db.getCollection(SOURCE_COLLECTION_NAME).findOneAndReplace(eq("_id", projectID), source);
		}
	}

	static Document findSource(int projectID) {
		FindIterable<Document> iterable = db.getCollection(SOURCE_COLLECTION_NAME).find(eq("_id", projectID));
		return iterable.first();
	}

	public long getSourcesSize() {
		return db.getCollection(SOURCE_COLLECTION_NAME).count();
	}

	//Report
	
	public static MongoCursor<Document> getCreatorIter() {
		FindIterable<Document> retrievedListOfCreators = getAllCreators();
		MongoCursor<Document> creatorDocIter = retrievedListOfCreators
				.iterator();
		return creatorDocIter;
	}


	public static void saveAnalysisReportToDB(JSONObject report, String creatorName) throws Exception {
		Document fullReportDoc = Document.parse(report.toJSONString());
		processLine(fullReportDoc, creatorName);
		
	}
	
	public static void processLine(Document fullReportDoc, String creatorName) throws Exception {
		int projectID = fullReportDoc.getInteger("_id");
		try {
			Document smells = (Document) fullReportDoc.get("smells");
			putAnalysisReport(projectID, smells);
			
			Document metrics = (Document) fullReportDoc.get("metrics");
			boolean containsMasteryMetric = metrics.containsKey("Mastery Level") || metrics.containsKey("MS");
			if(containsMasteryMetric && creatorName!=null){
				processCreatorRecord(projectID, creatorName, metrics);
			}
			
			putMetricsReport(projectID, metrics);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error reading project: " + projectID);
		}
	}

	private static void putMetricsReport(int projectID, Document metrics) {
		// TODO Auto-generated method stub
		
	}


	public static void putAnalysisReport(int projectID, Document report) {
		report.put("_id", projectID);
		if(findSmellReport(projectID) == null){
			db.getCollection(REPORT_COLLECTION_NAME).insertOne(report);
		}else{
			db.getCollection(REPORT_COLLECTION_NAME).findOneAndReplace(eq("_id", projectID), report);
		}
	}


	public static Document findSmellReport(int projectID) {
		FindIterable<Document> iterable = db.getCollection(REPORT_COLLECTION_NAME).find(eq("_id", projectID));
		if(iterable==null){
			return null;
		}else{
			return iterable.first();
		}
	}




	private static void processCreatorRecord(int projectID, String creatorName,
			Document metrics) {
		Document masteryReport = (Document) metrics.get("Mastery Level");
		if(masteryReport==null){
			masteryReport = (Document) metrics.get("MS");
		}
		Creator creator = new Creator(creatorName);
		creator.setMasteryReport(masteryReport);
		//check if project is original
		putCreatorRecord(creator);
	}


	private static Document findMetadata(int projectID) {
		FindIterable<Document> iterable = db.getCollection(METADATA_COLLECTION_NAME).find(eq("_id", projectID));
		if(iterable==null){
			return null;
		}else{
			return iterable.first();
		}
	}


	public static List<Document> getOrderedProjectReportsCreatedBy(String creatorName) {
		FindIterable<Document> iterable = db.getCollection(METADATA_COLLECTION_NAME).find(eq("creator", creatorName));
		MongoCursor<Document> metadataIter = iterable.iterator();
		List<Document> projectMetadataOrdering = new ArrayList<>();
		List<Document> analysisReport = new ArrayList<>();
		while(metadataIter.hasNext()){
			projectMetadataOrdering.add(metadataIter.next());
		}
		Collections.sort(projectMetadataOrdering, new Comparator<Document>(){
			@Override
			public int compare(Document proj0, Document proj1) {
				Date proj0ModifiedDate = proj0.getDate("modifiedDate");
				Date proj1ModifiedDate = proj1.getDate("modifiedDate");
				return proj0ModifiedDate.compareTo(proj1ModifiedDate);
			}
		});
		
		for(Document metadata: projectMetadataOrdering){
			Document report = findSmellReport(metadata.getInteger("_id"));
			analysisReport.add(report);
		}
		
		return analysisReport;
	}
	
	public static MongoCursor<Document> getAllProjectSources() {
		FindIterable<Document> iterable = db.getCollection(SOURCE_COLLECTION_NAME).find();
		return iterable.iterator();
	}
	


	public static FindIterable<Document> getAllCreators() {
		FindIterable<Document> iterable = db.getCollection(CREATOR_COLLECTION_NAME).find();
		return iterable;
	}


	public static Document getDocumentStructure() {
		FindIterable<Document> iterable = db.getCollection(REPORT_COLLECTION_NAME).find();
		return iterable.first();
	}
	

}
