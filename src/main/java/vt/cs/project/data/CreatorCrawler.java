package vt.cs.project.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.parser.JSONParser;

import vt.cs.smells.datamanager.crawler.Crawler;
import vt.cs.smells.datamanager.crawler.Creator;
import vt.cs.smells.datamanager.crawler.ProjectMetadata;
import vt.cs.smells.datamanager.main.DatasetCrawl2;
import vt.cs.smells.datamanager.worker.AnalysisDBManager;

public class CreatorCrawler {

	private static final int offset = 10;
	private static final int MINIMUM_PROJECTS_CREATED = 20;
	private static final int MINIMUM_YEARS_OF_EXPERIENCE = 2;
	static JSONParser parser = new JSONParser();	
	static Logger logger = Logger.getLogger(CreatorCrawler.class);
	static List<String> crawlBetweenIDRangeNoDB(int start, int end, int limit) {
		int counter = 0;
		List<String> qualifiedCreators = new ArrayList<String>();
		for (int projectID = start; projectID < end; projectID++) {
			if (counter >= limit) {
				break;
			}

			if (Crawler.checkIfExistsAndShared(projectID) == false) {
				continue;
			}

			ProjectMetadata currentProjectMetadata = new ProjectMetadata(
					projectID);
			try {
				Crawler.retrieveProjectMetadata(currentProjectMetadata);
				String creatorName = currentProjectMetadata.getCreator();
				Creator creator = CreatorQuery
						.retrieveCreatorMetadata(creatorName);

				if (creator.getJoinDuration() < MINIMUM_YEARS_OF_EXPERIENCE) {
					continue;
				}

				List<Integer> created_projects = CreatorQuery
						.extractProjectList(creator);
				HashMap<Integer, ProjectMetadata> originalProjectList = CreatorQuery
						.extractOriginalProjectList(created_projects);

				if (originalProjectList.size() >= MINIMUM_PROJECTS_CREATED) {
					DataCollector.collectProjectsByCreator(creatorName, originalProjectList);
					//save creator after retrieve project list
					DatabaseManager.putCreatorRecord(creator);

					qualifiedCreators.add(creatorName);
					DataCollector.appendCreatorIfNotExist(creatorName);
					counter++;

					logger.info("Added " + creatorName);
					logger.info("Collected: " + qualifiedCreators.size());
					logger.info("Progress:" + 100 * qualifiedCreators.size()
							/ limit + "%");

					projectID = projectID + offset; // skip next hundred for
													// data variability
				}
			} catch (Exception e) {
				logger.error("Failed crawling for project ID:" + projectID, e);
			}
		}

		return qualifiedCreators;
	}
	
	public static void main(String[] args){
		DatabaseManager.configure(Constants.HOST_NAME, Constants.DBNAME);
		String filename = "creator_list";
		DataCollector.setOuputConfig(Constants.OUTPUTDIR, filename);
		CreatorCrawler.crawlBetweenIDRangeNoDB(Constants.START_PROJECT_ID, Constants.END_PROJECT_ID, 1000);
	}
}

