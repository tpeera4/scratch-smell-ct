package vt.cs.project.data;

import vt.cs.smells.datamanager.crawler.Crawler;
import vt.cs.smells.datamanager.crawler.Creator;
import vt.cs.smells.datamanager.crawler.ProjectMetadata;

public class ScratchProjectQuery {

	public String getCreatorOfProject(int projectID) throws Exception {
		ProjectMetadata projectMeta = new ProjectMetadata(projectID);
		Crawler.retrieveProjectMetadata(projectMeta);
		return projectMeta.getCreator();
		
	}
	
	


	
}
