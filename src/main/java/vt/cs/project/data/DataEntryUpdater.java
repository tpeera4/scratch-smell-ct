package vt.cs.project.data;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.bson.Document;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import vt.cs.smells.analyzer.AnalysisException;
import vt.cs.smells.analyzer.AnalysisManager;
import vt.cs.smells.analyzer.analysis.BlockRepertoireAnalyzer;
import vt.cs.smells.analyzer.nodes.ScratchProject;
import vt.cs.smells.analyzer.parser.ParsingException;
import vt.cs.smells.datamanager.crawler.Creator;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;

public class DataEntryUpdater {

	private static final String outputDir = "../StatisticalAnalysis/";
	static Logger logger = Logger.getLogger(DataEntryUpdater.class);
	static String[] ctConceptKeys = new String[] { "FlowControl",
			"abstraction", "DataRepresentation", "Synchronization", "Logic",
			"User Interactivity", "Parallelization" };

	public static void main(String[] args) {
		// new compute new analysis
		// add CC and Block Repertoire for each project
		DatabaseManager.configure(Constants.HOST_NAME, Constants.DBNAME);
		MongoCursor<Document> creatorIter = DatabaseManager.getCreatorIter();
		
//		String[] missed = new String[]{"littlezakki",
//			"tintom",
//			"mosestmnt78",
//			"slapperbob",
//			"rtdt",
//			"TailsShadowAshby",
//			"tucker206",
//			"SeafoamTheCatfish"};
//		for(String creatorName: missed){
//			try{
//				updateCreatorRecord(creatorName);
//				logger.info(creatorName);
//			}catch(Exception e){
//				e.printStackTrace();
//			}
//		}
		
		while (creatorIter.hasNext()) {
			Document creator = creatorIter.next();
			String creatorName = (String) creator.get("_id");
			List<Document> projectReports = DatabaseManager
					.getOrderedProjectReportsCreatedBy(creatorName);
		
			//recompute
//			updateAnalysisResult(projectReports);
//			augmentCumulativeRepertoire(projectReports);
			processCumulativeCT(projectReports);
//			logger.info(creatorName+"-->"+projectReports.size());
		}
	}
	
	private static void updateCreatorRecord(String creatorName){
		Creator creator = CreatorQuery
				.retrieveCreatorMetadata(creatorName);
		DatabaseManager.putCreatorRecord(creator);
	}

	private static void updateAnalysisResult(List<Document> projectReports) {
		for (Document report : projectReports) {
			int projectID = report.getInteger("_id");
			Document srcDoc = DatabaseManager.findSource(projectID);
			String src = srcDoc.getString("src");
			try {
				ScratchProject proj = ScratchProject.loadProject(src);
				// analysis
				AnalysisManager blockAnalyzer = new AnalysisManager();
				blockAnalyzer.analyze(proj);
				JSONObject newReport = blockAnalyzer.getConciseJSONReports();
				Document newReportDoc = Document.parse(newReport.toJSONString());
				DatabaseManager.putAnalysisReport(projectID, newReportDoc);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
	}

	public static void augmentCumulativeRepertoire(List<Document> projectReports) {
		Map<String, Map<String, Integer>> cumulativeConceptMap = BlockRepertoireAnalyzer
				.getEmptyConceptMap();

		for (Document report : projectReports) {
			int projectID = report.getInteger("_id");
			Document srcDoc = DatabaseManager.findSource(projectID);
			String src = srcDoc.getString("src");

			try {
				ScratchProject proj = ScratchProject.loadProject(src);
				// analysis
				BlockRepertoireAnalyzer analyzer = new BlockRepertoireAnalyzer();
				analyzer.setProject(proj);
				analyzer.analyze();

				// updated cumulative repertoire score
				Map<String, Integer> blockMap = analyzer.getBlockMap();
				cumulativeConceptMap = BlockRepertoireAnalyzer
						.updateBlockToConcept(cumulativeConceptMap,
								blockMap);

				// retrieve reportEntry and augment the entry
				Document cumulativeRepertoire = BlockRepertoireAnalyzer
						.computeScores(cumulativeConceptMap);
				Document reportEntry = DatabaseManager
						.findSmellReport(projectID);
				Document updatedEntry = reportEntry.append("cumulativeBR",
						cumulativeRepertoire);
				DatabaseManager.putAnalysisReport(projectID, updatedEntry);

			} catch (ParsingException e) {
				e.printStackTrace();
			} catch (AnalysisException e) {
				e.printStackTrace();
			} catch (ParseException e) {
				e.printStackTrace();
			}

		}
	}

	

	public static void processCumulativeCT(List<Document> projectReports) {
		Document cumulativeCT = new Document();
		for (int reportIdx = 0; reportIdx < projectReports.size(); reportIdx++) {
			Document reportEntry = projectReports.get(reportIdx);
			Integer projectID = reportEntry.getInteger("_id");

			// keep track cumulative max CT for each project
			Document metrics = (Document) reportEntry.get("metrics");
			Document masteryConcepts = (Document) metrics.get("MS");
			int total = 0;
			for (String key : masteryConcepts.keySet()) {
				int value = 0;
				if (cumulativeCT.containsKey(key)) {
					value = cumulativeCT.getInteger(key);
				}
				int max = Math.max(value, masteryConcepts.getInteger(key));
				total += max;
				cumulativeCT.put(key, max);
			}

			// updated
			Document updatedEntry = reportEntry.append("cumulativeCT",
					cumulativeCT);

			DatabaseManager.putAnalysisReport(projectID, updatedEntry);
		}
	}

}
