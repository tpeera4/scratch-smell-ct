package vt.cs.project.data;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class TestCreatorCrawler {
	@Ignore
	@Test
	public void testCrawlAndSaveQualifiedCreator() {
		List<String> qualifiedCreators = CreatorCrawler.crawlBetweenIDRangeNoDB(Constants.START_PROJECT_ID, Constants.END_PROJECT_ID, 1);
		assertEquals(1, qualifiedCreators.size());
	}

}
