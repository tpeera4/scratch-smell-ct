package vt.cs.project.data;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import vt.cs.smells.datamanager.crawler.Creator;
import vt.cs.smells.datamanager.crawler.ProjectMetadata;

public class TestCreatorQuery {
	CreatorQuery query;
	@Before
	public void setUp() throws Exception {
		query = new CreatorQuery();
		
	}

	@Test
	public void getJoinedYear() throws ParseException {
		String creatorName = "memowpie";
		Creator creator = query.retrieveCreatorMetadata(creatorName);
		System.out.println(creator.getJoinDuration());
		assertEquals(2.5,creator.getJoinDuration(), 0.01);
	}
	
	@Test
	public void getProjectList(){
		String creatorName = "memowpie";
		Creator creator = query.retrieveCreatorMetadata(creatorName);
		List<Integer> projectList = query.extractProjectList(creator);
		assertFalse(projectList.isEmpty());
	}
	
	@Test
	public void getOriginalProject(){
		String creatorName = "memowpie";
		Creator creator = query.retrieveCreatorMetadata(creatorName);
		List<Integer> projectIDList  = query.extractProjectList(creator);
		HashMap<Integer, ProjectMetadata> originalProjectList = query.extractOriginalProjectList(projectIDList);
		System.out.println(originalProjectList);
		assertFalse(originalProjectList.isEmpty());
	}

}
