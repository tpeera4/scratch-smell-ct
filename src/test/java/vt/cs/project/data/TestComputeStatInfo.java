package vt.cs.project.data;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.junit.Before;
import org.junit.Test;

public class TestComputeStatInfo {
	private ComputeStatInfo compute;
	List<Document> projectReports;
	@Before
	public void setUp() throws Exception {
		DatabaseManager.configureToTest();
		compute = new ComputeStatInfo();
		
	}

	@Test
	public void testComputePercentile() {
		
		Double percentile = compute.findPercentile(30.0,"LS");
		System.out.println(percentile);
	}
	
	@Test
	public void testComputeMeanValue(){
		String creatorName = "Atari764";
		int numBaselineProject = 5;
		String[] attributes = new String[]{"bloc"};
		List<Document> projectReports = DatabaseManager
				.getOrderedProjectReportsCreatedBy(creatorName);
		Map<String, Double> result = compute.calculateMean(creatorName, numBaselineProject, attributes, projectReports);
		System.out.println(result);
	}
	
	@Test
	public void testComputeSumValue(){
		String creatorName = "Atari764";
		List<Document> projectReports = DatabaseManager
				.getOrderedProjectReportsCreatedBy(creatorName);
		int numBaselineProject = 5;
		String[] attributes = new String[]{"bloc"};
		Map<String, Double> result = compute.calculateSum(creatorName, numBaselineProject, attributes, projectReports);
		System.out.println(result); 
	}
	
	@Test
	public void testComputeMedianValue(){
		String creatorName = "Atari764";
		List<Document> projectReports = DatabaseManager
				.getOrderedProjectReportsCreatedBy(creatorName);
		int numBaselineProject = 5;
		String[] attributes = new String[]{"bloc"};
		Map<String, Double> result = compute.calculateMedian(creatorName, numBaselineProject, attributes, projectReports);
		System.out.println(result); 
	}

}
