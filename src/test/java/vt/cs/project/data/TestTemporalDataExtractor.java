package vt.cs.project.data;

import static org.junit.Assert.*;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.junit.Before;
import org.junit.Test;

import vt.cs.smells.datamanager.crawler.Creator;

public class TestTemporalDataExtractor {
	DecimalFormat df = new DecimalFormat("#.##");
	TemporalDataExtractor extractor;
	private List<Double> values;
	private List<Document> projectReports;
	@Before
	public void setUp() throws Exception {
		DatabaseManager.configureToTest();
		extractor = new TemporalDataExtractor();
		String creatorName = "SpiritTracks123";
		values = extractor.extractSmellDensity(creatorName, "LS");

		projectReports = DatabaseManager
				.getOrderedProjectReportsCreatedBy(creatorName);
	}

	@Test
	public void testExtractAttributeValuesOverTimeForCreator() {

		assertEquals(projectReports.size(), values.size());
		for(Number d: values){
			System.out.println(df.format(d));
		}
	}
	
	@Test
	public void testCalculateProjectNumberWhenHigherThanThreshold(){
		String creatorName = "SpiritTracks123";
		ComputeStatInfo compute = new ComputeStatInfo();
		Double pctLS = compute.findPercentile(70.0,"LS");
		Double pctDC = compute.findPercentile(70.0,"DC");
		int windowSize = 5;
		System.out.println(pctLS);
		System.out.println(pctDC);
		Map<String, Double> thresholdMap = new HashMap<>();
		thresholdMap.put("LS", pctLS);
		thresholdMap.put("DC", pctDC);
		String[] attributes = new String[]{"LS","DC"};
		Map<String,List<Integer>> projectNumLowRisk = extractor.getProjectNumberHigherThanThreshold(creatorName, attributes,thresholdMap, windowSize);
		System.out.println(projectNumLowRisk);
		
		
	}
	
	
	
	

}
