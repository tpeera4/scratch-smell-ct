package vt.cs.project.data;

import static org.junit.Assert.*;

import org.bson.Document;
import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;

import vt.cs.smells.analyzer.AnalysisManager;
import vt.cs.smells.datamanager.crawler.Crawler;
import vt.cs.smells.datamanager.crawler.Creator;
import vt.cs.smells.datamanager.crawler.ProjectMetadata;

public class TestDataEntryUpdater {
	
	CreatorQuery query;
	
	@Before
	public void setUp() throws Exception {
		DatabaseManager.configureToTest();
		query = new CreatorQuery();
	}

	@Test
	public void testUpdateSpecificKeyValueOfAnalysisReportEntryToDB() throws Exception {
		int projectID = 99926345;

		// Save full analysis entry
		ProjectMetadata metadata = new ProjectMetadata(projectID);
		Crawler.retrieveProjectMetadata(metadata);
		String creatorName = metadata.getCreator();
		String src = Crawler.retrieveProjectSourceFromProjectID(projectID);
		AnalysisManager analyzer = new AnalysisManager();
		analyzer.analyze(src);
		JSONObject report = analyzer.getConciseJSONReports();
		DatabaseManager.saveAnalysisReportToDB(report, creatorName);
		
		// Retrieve the entry for modification
		Document analysisReportEntry = DatabaseManager.findSmellReport(projectID);
		Document updatedEntry = analysisReportEntry.append("somethingElse", 5);
		
		// Update a particular key value
		DatabaseManager.putAnalysisReport(projectID, updatedEntry);
		// Retrieve to check if it's updated
		Document entryToCheck = DatabaseManager.findSmellReport(projectID);
		assertEquals(entryToCheck,updatedEntry);
	}

}
