package vt.cs.project.data;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.bson.Document;
import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import vt.cs.smells.analyzer.AnalysisManager;
import vt.cs.smells.datamanager.crawler.Crawler;
import vt.cs.smells.datamanager.crawler.Creator;
import vt.cs.smells.datamanager.crawler.ProjectMetadata;

public class TestSaveDataToDB {
	boolean test = true;
	CreatorQuery query;
	@Before
	public void setUp() throws Exception {
		DatabaseManager.configureToTest();
		query = new CreatorQuery();
	}
	
	@After
	public void tearDown(){
//		dbmanager.dropDatabase();
	}

	@Test
	public void testSaveCreatorMetadataToDB() {
		String creatorName = "memowpie";
		Creator creator = CreatorQuery.retrieveCreatorMetadata(creatorName);
		DatabaseManager.putCreatorRecord(creator);
	}
	
	@Test
	public void testSaveProjectMetadataToDB() {
		String creatorName = "memowpie";
		Creator creator = CreatorQuery.retrieveCreatorMetadata(creatorName);
		List<ProjectMetadata> projectMetadataList = creator.getProjectList();
		for(ProjectMetadata metadata : projectMetadataList){
			DatabaseManager.putProjectMetadataRecord(metadata);
		}
	}
	
	@Test
	public void testSaveProjectSourceToDB() {
		String creatorName = "memowpie";
		Creator creator = CreatorQuery.retrieveCreatorMetadata(creatorName);
		List<ProjectMetadata> projectMetadataList = creator.getProjectList();

		for(ProjectMetadata metadata : projectMetadataList){
			try {
				String src = Crawler.retrieveProjectSourceFromProjectID(metadata.getProjectID());
				DatabaseManager.putSource(metadata.getProjectID(), src);
			} catch (Exception e) {}
		}
	}
	
	@Test
	public void testSaveAnalysisReportToDB() throws Exception {
		int projectID = 99926345;
		ProjectMetadata metadata = new ProjectMetadata(projectID);
		Crawler.retrieveProjectMetadata(metadata);
		String creatorName = metadata.getCreator();
		String src = Crawler.retrieveProjectSourceFromProjectID(projectID);
		AnalysisManager analyzer = new AnalysisManager();
		analyzer.analyze(src);
		JSONObject report = analyzer.getConciseJSONReports();
		DatabaseManager.saveAnalysisReportToDB(report, creatorName);
		System.out.println(report);
		
	}
	
	@Test
	public void testRetrieveAnalysisReportForSpecifiedCreatorFromDB() {
		String creatorName = "memowpie";
		List<Document> projects = DatabaseManager.getOrderedProjectReportsCreatedBy(creatorName);
		for(Document proj: projects){
			System.out.println(proj);
		}
	}
	
	
	
	

}
