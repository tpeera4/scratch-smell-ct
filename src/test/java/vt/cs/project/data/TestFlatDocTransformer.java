package vt.cs.project.data;

import static org.junit.Assert.*;

import org.bson.Document;
import org.junit.Before;
import org.junit.Test;

public class TestFlatDocTransformer {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		DatabaseManager.configureToTest();
		Document analysisReport = DatabaseManager.findSmellReport(49004598);
		Document metaReport = DatabaseManager.findProjectMetadata(49004598);
		FlatDocTransformer transformer = new FlatDocTransformer();
		Document flatAnalysisReport= transformer.flattenAnalysis(analysisReport);
		Document flatMetaReport= transformer.flattenMeta(metaReport);
		Document flatReport = transformer.merge(flatAnalysisReport, flatMetaReport);
		System.out.println(flatReport);
	}

}
