package vt.cs.project.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bson.Document;
import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;

import vt.cs.smells.analyzer.AnalysisManager;
import vt.cs.smells.datamanager.crawler.Crawler;
import vt.cs.smells.datamanager.crawler.Creator;
import vt.cs.smells.datamanager.crawler.ProjectMetadata;

public class TestStatsCompute {

	private StatsCompute statsCompute;
	private List<Document> reports;

	@Before
	public void setUp() throws Exception {
		int projectID1 = 41607672;
		int projectID2 = 25195961;
		String src1 = Crawler.retrieveProjectSourceFromProjectID(projectID1);
		String src2 = Crawler.retrieveProjectSourceFromProjectID(projectID2);
		AnalysisManager analyzer1 = new AnalysisManager();
		analyzer1.analyze(src1);
		AnalysisManager analyzer2 = new AnalysisManager();
		analyzer2.analyze(src2);
		statsCompute = new StatsCompute();
		Document report1 = Document.parse(analyzer1.getConciseJSONReports().toJSONString());
		Document report2 = Document.parse(analyzer2.getConciseJSONReports().toJSONString());
		reports = new ArrayList<Document>();
		reports.add(report1);
		reports.add(report2);
		
		for(Document report:reports){
			System.out.println(report);
		}
		statsCompute.setInputProjects(reports);
	}

//	@Test
//	public void computeCTMaxForASetOfProjectReports	() throws Exception {
//		Document doc = statsCompute.getMaxForCT();
//		System.out.println(doc);
//	}
	
	@Test
	public void computeSmellMedianForASetOfProjectReports () {
		Document doc = statsCompute.getMedianForSmells();
		System.out.println(doc);
	}
	
//	@Test
//	public void computeCTMaxForLargeSetOfProjectReports () {
//		statsCompute = new StatsCompute();
//		String creatorName = "Atari764";
//		Creator creator = CreatorQuery.retrieveCreatorMetadata(creatorName);
//		
//		projectMetadataList = CreatorQuery.extractOriginalProjectList(creator.getName());
//		List<Document> reportLists = new ArrayList<Document>();
//		for(ProjectMetadata metadata : projectMetadataList){
//			try {
//				String src = Crawler.retrieveProjectSourceFromProjectID(metadata.getProjectID());
//				AnalysisManager analyzer = new AnalysisManager();
//				analyzer.analyze(src);
//				Document report = Document.parse(analyzer.getConciseJSONReports().toJSONString());
//				reportLists.add(report);
//			} catch (Exception e) {}
//			
//			statsCompute.setInputProjects(reportLists);
//		}
//		
//		Document smellMedian = statsCompute.getMedianForSmells();
//		Document ctMax = statsCompute.getMaxForCT();
//		System.out.println(ctMax);
//		System.out.println(smellMedian);
//	}
	
	
//	@Test
//	public void orderProjectMetadataByDateShared(){
//		statsCompute = new StatsCompute();
//		String creatorName = "Kidplayer111";
//		Creator creator = CreatorQuery.retrieveCreatorMetadata(creatorName);
////		CreatorQuery.retrieveCreatorProjectList(creator);
//		List<ProjectMetadata> projectMetadataList = creator.getProjectList();
//		Collections.sort(projectMetadataList);
//		List<Document> reportLists = new ArrayList<Document>();
//		for(ProjectMetadata meta: projectMetadataList){
////			System.out.println(meta.getTitle());
////			System.out.println(meta.getDateShared());
//			
//			
//			try {
//				String src = Crawler.retrieveProjectSourceFromProjectID(meta.getProjectID());
//				AnalysisManager analyzer = new AnalysisManager();
//				analyzer.analyze(src);
//				Document report = Document.parse(analyzer.getConciseJSONReports().toJSONString());
//				//ct
//				Document metrics = (Document) report.get("metrics");
//				Document MS = (Document) metrics.get("MS");
//				Document PS = (Document) metrics.get("PS");
//				double bloc = PS.getDouble("bloc");
//				if(bloc >100){
//					System.out.println(MS.get("total"));
//				}
//				
//				
//
//			} catch (Exception e) {}
//		}
//	}
}
